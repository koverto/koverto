//! Functions for the command line interface.

use std::path;

pub mod mta;
pub mod subcommands;

// The following is a comment and not documentation because it would then be
// used as an `about` parameter and printed in the help.
// Instead, let the help print the `Cargo.toml` description, by not adding
// documentation to this struct.
// The command line struct with arguments and subcommands.
#[derive(Clap, Debug, PartialEq, Eq)]
// The name, version and description are parsed from the manifest.
pub struct Opt {
    // It is still not possible to use `default_value` with function or
    // literal.
    #[clap(
        short = 'l',
        long = "log-level",
        about = "Set log level to trace, debug, info, warn or error"
    )]
    pub log_level: Option<String>,

    #[clap(short = 'c', long = "config", about = "Custom config file path.")]
    pub config: Option<path::PathBuf>,

    #[clap(subcommand)]
    pub subcommand: Option<SubCommand>,
}

/// The command line subcommands.
#[derive(Clap, Debug, PartialEq, Eq)]
pub enum SubCommand {
    // `name` is needed to do not have to type subcommands with initial
    // capital letter.
    #[clap(
        name = "import",
        about = "Import a keyring file into the keys store."
    )]
    // Instead of creating a wrapper to an struct, create the struct as part of
    // the enum.
    // But it can not be a wrapper to a String or PathBuff
    Import {
        #[clap(parse(from_os_str))]
        keyring_path: path::PathBuf,
    },
    #[clap(name = "delete-store", about = "Delete the key store.")]
    DeleteStore,
    #[clap(name = "delete-key", about = "Delete a key from the key store.")]
    DeleteKey { key: String },
    #[clap(name = "list-keys", about = "List the keys in the store.")]
    ListKeys,
    #[clap(name = "mta", about = "Run the receive-only MTA.")]
    Mta,
}
