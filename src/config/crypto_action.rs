use serde::{Deserialize, Serialize};

/// Actions to perform on an incoming email.
///
/// Depending on the configuration different policies
/// for constructing mails are possible:
/// * `MustEncrypt` - always encrypt and sign
/// * `Fallback` - try to encrypt, sign only if it fails
/// * `SignOnly` - only sign the mails.
///
#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
pub enum CryptoAction {
    SignOnly,
    Fallback,
    MustEncrypt,
}

/// Default email action is to encrypt
impl Default for CryptoAction {
    fn default() -> Self {
        CryptoAction::MustEncrypt
    }
}
