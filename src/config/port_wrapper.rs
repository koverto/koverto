use serde::{Deserialize, Serialize};
use std::fmt;

use crate::constants::RECEIVE_MTA_PORT;

/// `u32` wrapper used in
/// [ReceiveMTA.port](struct.ReceiveMTA.html#structfield.port) and
/// [SendMTA.port](struct.SendMTA.html#structfield.port)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct PortWrapper(pub u32);

impl Default for PortWrapper {
    fn default() -> Self {
        PortWrapper(RECEIVE_MTA_PORT)
    }
}

impl fmt::Display for PortWrapper {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}
