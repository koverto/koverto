//! Configuration of the entire service.
//!
//! Configure service parameters such as
//!  * outgoing SMTP server
//!  * logging
//!  * paths
//!  * different clients
//!
//! One instance of koverto can be configured
//! to handle multiple clients in different ways.
//! The Client configuration allows setting
//!  * the directory to watch for incoming mails
//!  * the expected sender email address
//!  * the policy (CryptoAction) to follow.
//!
//! Implemented as structs with defaults and (de)serialization.

use serde::{Deserialize, Serialize};

use anyhow::{Context, Result};
use std::fmt;
use std::fs;
use std::io;
use std::path;

use super::constants::*;

mod client;
mod clients;
mod crypto_action;
mod home_path;
mod level_filter_wrapper;
mod log_module;
mod paths;
mod port_wrapper;
mod receive_mta;
mod send_mta;
mod store_config;

pub use client::Client;
pub use client::ClientFromWrapper;
pub use clients::Clients;
pub use crypto_action::CryptoAction;
use home_path::HomePath;
pub use level_filter_wrapper::LevelFilterWrapper;
use log_module::LogModule;
pub use paths::default_config_path;
pub use port_wrapper::PortWrapper;
pub use receive_mta::ReceiveMTA;
pub use send_mta::SendMTA;
pub use store_config::StoreConfig;

/// Structure to parse/build the configuration.
#[derive(Serialize, Deserialize, Debug, PartialEq, Default)]
pub struct Config {
    #[serde(default)]
    pub log_level: LevelFilterWrapper,
    #[serde(default)]
    pub log_module: LogModule,
    #[serde(default)]
    /// The filesystem path directory that will be the root for other data
    /// data directories. By default is the user home.
    pub home_path: HomePath,

    #[serde(default)]
    pub store_config: StoreConfig,
    #[serde(default)]
    pub receive_mta: ReceiveMTA,
    #[serde(default)]
    pub send_mta: SendMTA,
    #[serde(default)]
    pub clients: Clients,
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", serde_json::to_string_pretty(&self).unwrap())
    }
}

impl<P> From<P> for Config
where
    P: AsRef<path::Path>,
{
    fn from(user_home: P) -> Self {
        let mut cfg = Config::default();
        cfg.home_path = HomePath(user_home.as_ref().to_path_buf());
        cfg.store_config = StoreConfig::from(&cfg.home_path);
        cfg.receive_mta = ReceiveMTA::from(&cfg.home_path);
        cfg
    }
}

/// Open, read and parse a configuration file formatted as `toml`
impl Config {
    pub fn new() -> Self {
        let mut cfg = Config::default();
        cfg.store_config = StoreConfig::from(&cfg.home_path);
        cfg.receive_mta = ReceiveMTA::from(&cfg.home_path);
        cfg
    }

    /// Load config file.
    ///
    /// If no filename is given - load the default config file.
    ///
    /// returns
    /// Ok(Some(Config))
    /// Ok(None) - no config path given, no default config found
    /// Err(err) - io or parser errors
    pub fn load<P>(filename: &Option<P>, home: &P) -> Result<Option<Config>>
    where
        P: AsRef<path::Path>,
    {
        match filename {
            Some(config_path) => {
                Ok(Some(Config::load_file(&config_path).context(format!(
                    "Error loading configuration {:?}",
                    config_path.as_ref()
                ))?))
            }
            None => Config::load_default_config_file(home).context(format!(
                "Error loading default config file {:?}",
                default_config_path(home)
            )),
        }
    }

    pub fn data_path(&self) -> path::PathBuf {
        let dp = paths::data_path_from_home(&self.home_path);
        Config::create_dir(&dp);
        dp
    }

    pub fn keyring_path(&self) -> path::PathBuf {
        self.data_path().join(KEYRING_FILE)
    }

    pub fn secret_key_path<S>(&self, sender: S) -> path::PathBuf
    where
        S: AsRef<str>,
    {
        self.data_path().join(paths::secret_key_file_from(sender))
    }

    pub fn email_path(&self) -> path::PathBuf {
        let ep = self.home_path.join(EMAIL_DIR);
        Config::create_dir(&ep);
        ep
    }

    pub fn outgoing_email_path(&self) -> path::PathBuf {
        let out = self.home_path.join(OUTGOING_EMAIL_DIR);
        Config::create_dir(&out);
        out
    }

    fn load_file<P>(config_path: &P) -> Result<Config>
    where
        P: AsRef<path::Path>,
    {
        Config::from_str(&fs::read_to_string(config_path.as_ref())?)
    }

    fn load_default_config_file<P>(home: &P) -> Result<Option<Config>>
    where
        P: AsRef<path::Path>,
    {
        match fs::read_to_string(default_config_path(home)) {
            Ok(string) => Ok(Some(Config::from_str(&string)?)),
            Err(err) if err.kind() == io::ErrorKind::NotFound => Ok(None),
            Err(err) => Err(err.into()),
        }
    }

    fn from_str(cfg_string: &str) -> Result<Config> {
        let mut cfg: Config = toml::from_str(cfg_string)?;
        if cfg.store_config.home.to_str().unwrap().is_empty() {
            cfg.store_config.home =
                store_config::store_path_from_home(&cfg.home_path)
        };
        if cfg
            .receive_mta
            .tls_certificate_path
            .to_str()
            .unwrap()
            .is_empty()
        {
            cfg.receive_mta.tls_certificate_path =
                cfg.home_path.join(DATA_DIR).join(RECEIVE_MTA_TLS_CERT_FILE)
        };
        Ok(cfg)
    }

    fn create_dir(dir: &path::PathBuf) {
        if fs::create_dir_all(&dir).is_err() {
            error!(
                "Can not create directory {}",
                dir.as_path().to_str().unwrap()
            );
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tests_common::fixture;
    use std::io::prelude::*;

    #[test]
    fn config_new() {
        let cfg = Config::new();
        assert_eq!(cfg.store_config.home, store_config::default_store_path());
        assert_eq!(&cfg.clients.first().unwrap().directory, "mail");
    }

    #[test]
    fn secret_key_path() {
        let cfg = Config::new();
        let skf = cfg.secret_key_path(CLIENT_FROM);
        let expected = paths::user_home()
            .join(DATA_DIR)
            .join("application@localhost.localdomain_secret.pgp");
        assert_eq!(skf, expected);
    }

    #[test]
    fn outgoing_email_path() {
        let cfg = Config::new();
        let out = cfg.outgoing_email_path();
        let expected = paths::user_home().join("outgoing");
        assert_eq!(out, expected);
        assert!(out.exists());
    }

    #[test]
    fn config_load() {
        let cfg = Config::load(&None, &fixture::tests_home_dir()).unwrap();
        // Not using `::default()`` to make clearer which the values are
        let expected_cfg = Config {
            log_level: LevelFilterWrapper(LOG_LEVEL),
            log_module: LogModule(LOG_MODULE.to_owned()),
            home_path: HomePath(fixture::tests_home_dir()),
            store_config: StoreConfig::from(fixture::tests_home_dir()),
            receive_mta: ReceiveMTA::from(fixture::tests_home_dir()),
            send_mta: SendMTA {
                domain: SEND_MTA_DOMAIN.to_owned(),
                port: PortWrapper(SEND_MTA_PORT),
                credentials: None,
            },
            clients: {
                Clients(vec![Client::from("wiki@localhost.localdomain")])
            },
        };
        assert_eq!(cfg, Some(expected_cfg));
    }

    #[test]
    fn config_err_in_default_file() -> Result<()> {
        let home = fixture::tmp_home()?;
        let config_file = default_config_path(&home);
        let mut config_file = fs::File::create(config_file)
            .context("Creating config file for test")?;
        config_file
            .write_all(b"invalid config toml")
            .context("Writing invalid config file for test")?;
        let cfg = Config::load(&None, &home);
        assert!(cfg.is_err());
        let err = cfg.err().unwrap();
        let message = err.to_string();
        assert!(message.starts_with("Error loading default config file"));
        let details = err.root_cause().to_string();
        assert_eq!(
            details,
            "expected an equals, found an identifier at line 1 column 9"
        );
        Ok(())
    }

    #[test]
    fn multi_client() {
        let cfg = Config::load(
            &Some(fixture::tests_home_join("multiclient.toml")),
            &fixture::tests_home_dir(),
        )
        .unwrap()
        .unwrap();
        let mut iter = cfg.clients.iter();
        let first = iter.next().unwrap();
        let second = iter.next().unwrap();
        assert_eq!(&first.directory, "mail/must_encrypt");
        assert_eq!(&second.directory, "mail/sign_only");
        assert_eq!(
            first.crypto_action,
            crypto_action::CryptoAction::MustEncrypt
        );
        assert_eq!(
            second.crypto_action,
            crypto_action::CryptoAction::SignOnly
        );
    }
}
