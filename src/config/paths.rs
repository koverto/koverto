use crate::constants::{
    CONFIG_FILE, DATA_DIR, KEY_FILE_EXTENSION, SECRET_KEY_SUFFIX,
};
use std::env;
use std::path;

// This functions are useful to do not have to repeat the serde defaults
// for the keys and struct Default implementation
// derive Default can not be used even if all keys have serde defaults because
// they are for serde.
// It would not be needed using https://github.com/TedDriggs/serde_default,
// nor having to implement Default

pub fn user_home() -> path::PathBuf {
    path::PathBuf::from(&env::var("HOME").unwrap())
}

pub fn default_config_path<P>(home: &P) -> path::PathBuf
where
    P: AsRef<path::Path>,
{
    home.as_ref().join(CONFIG_FILE)
}

pub fn default_data_dir() -> path::PathBuf {
    user_home().join(DATA_DIR)
}

pub fn data_path_from_home<P: AsRef<path::Path>>(
    home_path: P,
) -> path::PathBuf {
    home_path.as_ref().join(DATA_DIR)
}

pub fn secret_key_file_from<S>(sender: S) -> path::PathBuf
where
    S: AsRef<str>,
{
    path::PathBuf::from(&format!(
        "{}{}{}",
        sender.as_ref(),
        SECRET_KEY_SUFFIX,
        KEY_FILE_EXTENSION
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::constants::CLIENT_FROM;

    #[test]
    fn secret_key_file_from_sender() {
        let skf = secret_key_file_from(CLIENT_FROM);
        let file = "application@localhost.localdomain_secret.pgp";
        assert_eq!(path::PathBuf::from(file), skf);
    }
}
