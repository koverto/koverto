use crate::config::Client;
use crate::config::CryptoAction;
use serde::{Deserialize, Serialize};
use std::ops::Deref;

/// [Client](struct.Client.html) vector wrapper used in
/// [Config.clients](struct.Config.html#structfield.clients)
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Clients(pub Vec<Client>);

impl Clients {
    pub fn new(clients: Vec<Client>) -> Self {
        Clients(clients)
    }

    fn client_with_from<S>(&self, sender: S) -> Option<&Client>
    where
        S: AsRef<str>,
    {
        for client in self.iter() {
            if client.from.as_str() == sender.as_ref() {
                return Some(client);
            }
        }
        None
    }

    pub fn client_with_from_action<S>(&self, sender: S) -> Option<CryptoAction>
    where
        S: AsRef<str>,
    {
        match self.client_with_from(sender) {
            Some(client) => Some(client.crypto_action.clone()),
            None => None,
        }
    }

    pub fn client_with_from_send<S>(&self, sender: S) -> bool
    where
        S: AsRef<str>,
    {
        match self.client_with_from(sender) {
            Some(client) => client.send_all_or_none,
            None => false,
        }
    }
}

impl Default for Clients {
    fn default() -> Self {
        Clients(default_clients())
    }
}

impl Deref for Clients {
    type Target = Vec<Client>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

fn default_clients() -> Vec<Client> {
    vec![Client::default()]
}
