//! Observe an email queue and handle the arriving emails.
//!
//! Uses Inotify to watch a directory.
//! All files that are created or moved there
//! will be parsed as emails and processed.
//!
//! Performs all steps needed to handle an incoming email:
//! * parse the mail
//! * create outgoing mails for all recipients (via Prepare)
//! * send these mails (via Deliver)

use crate::constants::EMAIL_EXT;
use crate::email::Deliver;
use crate::email::Prepare;
use anyhow::Result;
use std::{fs, path};

// buffer size taken from inotify.rs README
// each event is ~ 16 byte + size_of(filename)
// assuming 16 byte filenames
// this is enough for 4096/32 = 128 events
const INOTIFY_BUFFER_SIZE: usize = 4096;

pub struct Queue {
    preparation: Box<dyn Prepare>,
    delivery: Box<dyn Deliver>,
    path: path::PathBuf,
}

impl Queue {
    pub fn new<P>(
        path: P,
        preparation: Box<dyn Prepare>,
        delivery: Box<dyn Deliver>,
    ) -> Self
    where
        P: AsRef<path::Path>,
    {
        Self {
            path: path.as_ref().to_path_buf(),
            preparation,
            delivery,
        }
    }

    /// Continously look for mails in queue dir and process them.
    ///
    /// For now the processing happens syncronously.
    /// #43 tracks creating a multi-threated approach.
    pub fn watch(&self) -> Result<()> {
        let inotify_result = self.start_watching();
        self.process_existing_files()?;
        // check for inotify errors now that existing files have been processed
        let mut inot = inotify_result?;
        let mut buffer = [0u8; INOTIFY_BUFFER_SIZE];
        loop {
            let events = inot.read_events_blocking(&mut buffer)?;
            self.process_events(events);
        }
    }

    fn start_watching(&self) -> Result<inotify::Inotify> {
        let mut inot = inotify::Inotify::init()?;
        let mask =
            inotify::WatchMask::CLOSE_WRITE | inotify::WatchMask::MOVED_TO;
        inot.add_watch(&self.path, mask)?;
        Ok(inot)
    }

    /// Process all the Emails in the queue directory.
    ///
    /// This is intended to be used for a first pass
    /// that processes all files that were created before koverto ran.
    ///
    /// It does not detect files that are still open.
    /// So when run while files are still being written
    /// it may process them too early.
    fn process_existing_files(&self) -> Result<()> {
        debug!(
            "Processing queue directory {}",
            self.path.as_path().to_str().unwrap()
        );
        let dir = fs::read_dir(&self.path)?;
        for entry in dir {
            if entry.is_err() {
                error!("{:?}", entry.err());
                continue;
            }
            let file = entry.unwrap();
            if !file.file_type()?.is_file() {
                continue;
            }
            if !file.file_name().to_str().unwrap().ends_with(EMAIL_EXT) {
                continue;
            }
            if let Err(err) = self.process_file(&file.path()) {
                error!("Error reading file {:?}: {:?}", &file.path(), err);
            }
        }
        Ok(())
    }

    fn process_events(&self, events: inotify::Events) {
        for event in events {
            debug!("{:?}", event);
            if let Some(name) = event.name {
                debug!("trying to process {:?}", name);
                let path = self.path.join(name);
                let result = self.process_file(&path);
                debug!("processed {:?}", name);
                // log error but proceed with the other events
                if result.is_err() {
                    error!("Error processing {:?}.", &path);
                }
            }
        }
    }

    fn process_file<F>(&self, file: F) -> Result<()>
    where
        F: AsRef<path::Path> + std::fmt::Debug,
    {
        trace!("Found email file {:?}", file.as_ref());
        // Ignore one single email fail to be read, but log it.
        let s = fs::read_to_string(&file)?;
        match self.handle(s) {
            Ok(()) => fs::remove_file(&file)?,
            Err(err) => self.handle_err(&file, err)?,
        };
        Ok(())
    }

    fn handle(&self, s: String) -> Result<()> {
        trace!("Processing email: {}", &s);
        let sendables = self.preparation.prepare(s)?;
        debug!("Successfully build {} sendables.", sendables.len());
        self.delivery.deliver(sendables);
        Ok(())
    }

    fn handle_err<F>(&self, file: F, err: anyhow::Error) -> Result<()>
    where
        F: AsRef<path::Path> + std::fmt::Debug,
    {
        let filename = file.as_ref().file_name().unwrap();
        let path = self.path.join("err");
        if !path.exists() {
            fs::create_dir(&path)?;
        }
        error!("Failed to parse {:?}: {:?}", file, err);
        fs::rename(&file, path.join(filename))?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::Queue;
    use crate::email::Deliver;
    use crate::email::Prepare;
    use crate::{tests_common::fixture, Result};
    use std::{fs, path};

    struct Ignore {}

    impl Prepare for Ignore {
        fn prepare(&self, _in: String) -> Result<Vec<lettre::SendableEmail>> {
            Ok(vec![])
        }
    }

    impl Deliver for Ignore {
        fn deliver(&self, _sendable_emails: Vec<lettre::SendableEmail>) {}
    }

    #[test]
    fn process_existing_files() {
        let home = fixture::tmp_home_with_keys().unwrap();
        let dir = create_queue(&home).unwrap();
        let preparation = Box::new(Ignore {});
        let delivery = Box::new(Ignore {});
        let queue = Queue::new(dir.clone(), preparation, delivery);
        let result = queue.process_existing_files();
        assert!(result.is_ok());
        // After processing the queue, it should be empty.
        let mut dir_entries: fs::ReadDir = fs::read_dir(dir).unwrap();
        assert!(dir_entries.next().is_none());
    }

    fn create_queue<P: AsRef<path::Path>>(home: P) -> Result<path::PathBuf> {
        let queue = home.as_ref().join("mail");
        fs::create_dir_all(&queue)?;
        for i in 0..100 {
            fs::copy(
                fixture::tests_data_join("email_plain.eml"),
                queue.join(format!("{}.eml", i)),
            )?;
        }
        Ok(queue)
    }
}
