use anyhow::{Context, Result};
use std::str::FromStr;

use crate::email::Error;

/// Data from parsing the original Email
#[derive(Debug, Clone)]
pub struct Incoming {
    from: String,
    date: String,
    subject: String,
    message: ::email::MimeMessage,
    recipients: Vec<String>,
}

impl Incoming {
    /// Parses the string as a mime message.
    ///
    /// Will fail on parse errors and missing fields.
    ///
    /// In particular `from` and `date` are required.
    pub fn new<S: AsRef<str>>(email: S) -> Result<Self> {
        let incoming = email
            .as_ref()
            .parse::<Self>()
            .context("Error parsing Email")?;
        Ok(incoming)
    }

    /// Get email sender address from `from` header.
    ///
    /// ([RFC2822]) lists a number of rarely used other cases:
    /// * `trace` and `resent` headers
    /// * multiple `from` headers or multiple entries in the `from` header.
    /// * `sender` header (which would be relevant with multiple `from`s).
    ///
    /// We currently do not handle those.
    ///
    pub fn from(&self) -> &str {
        &self.from
    }

    /// Subject of the incoming message or "No Subject".
    pub fn subject(&self) -> &str {
        &self.subject
    }

    /// Date of the given message.
    pub fn date(&self) -> &str {
        &self.date
    }

    pub fn message(&self) -> &::email::MimeMessage {
        &self.message
    }

    /// Part of the message that will be encrypted.
    ///
    /// For now we just copy over the Content-Type, Body and Children.
    /// TODO: copy all mime part headers (starting with "Content-").
    /// TODO: Convert to 7-bit if needed.
    /// This is also where one would add protected headers etc.
    pub fn plaintext(&self) -> ::email::MimeMessage {
        let mut full = self.message().clone();
        let mut plaintext = ::email::MimeMessage::new_blank_message();
        plaintext.headers.insert(
            full.headers
                .get("Content-Type".into())
                .cloned()
                .unwrap_or_else(|| {
                    ::email::Header::new(
                        "Content-Type".into(),
                        "text/plain".into(),
                    )
                }),
        );
        std::mem::swap(&mut plaintext.children, &mut full.children);
        std::mem::swap(&mut plaintext.body, &mut full.body);
        assert_eq!(full.children.len(), 0);
        plaintext
    }

    pub fn message_type(&self) -> Option<::email::MimeMultipartType> {
        self.message.message_type
    }

    pub fn body(&self) -> &str {
        &self.message.body
    }

    /// Email addresses of all recipients in the `To` field.
    ///
    /// We currently do not handle `Cc` and `Bcc` fields.
    pub fn recipients(&self) -> Vec<String> {
        self.recipients.clone()
    }
}

impl FromStr for Incoming {
    type Err = anyhow::Error;
    // #2: hide the subject
    fn from_str(src: &str) -> Result<Self> {
        let message = ::email::MimeMessage::parse(src)?;
        Ok(Self {
            from: parse_sender_address(&message)?,
            recipients: parse_recipients_addresses(&message)?,
            date: parse_date(&message)?,
            subject: parse_subject(&message)?,
            message,
        })
    }
}

/// Parse date of the given message.
///
/// # Return Value
///
/// `Ok<String>` if a valid header is found.
/// `Err<NoDateError>` or `Err<InvalidDateError>` otherwise.
fn parse_date(mime: &::email::MimeMessage) -> Result<String> {
    match mime.headers.get("Date".to_owned()) {
        Some(date) => Ok(date.get_value().or(Err(Error::InvalidDateError))?),
        None => Err(Error::NoDateError.into()),
    }
}

/// Subject of the given message.
///
/// # Return Value
///
/// `Ok<String>` if a valid header is found.
/// `Ok<"No Subject">` if the subject was empty.
/// `Err<::email::ParsingError>` if parsing failed.
fn parse_subject(mime: &::email::MimeMessage) -> Result<String> {
    match mime.headers.get("Subject".to_owned()) {
        Some(header) => Ok(header.get_value()?),
        None => Ok("No Subject".to_string()),
    }
}

/// Returns a vector of email addresses
///
/// Parse as specified in RFC2822
/// from the recipients fields in a email message.
fn parse_recipients_addresses(
    mime: &::email::MimeMessage,
) -> Result<Vec<String>> {
    trace!("Obtaining recipients' email addresses...");

    // #4: Cc could be parsed too
    // #5: Bcc would require to encrypt separately.
    let addresses: Vec<::email::Address> = mime
        .headers
        .get("To".to_owned())
        .ok_or(Error::NoToError)?
        .get_value()?;
    let plain_email_addresses: Vec<String> = addresses
        .iter()
        .map(|address| match address.clone() {
            ::email::Address::Group(_, mailboxes) => mailboxes,
            ::email::Address::Mailbox(mailbox) => vec![mailbox],
        })
        .flatten()
        .map(|mailbox| mailbox.address)
        .collect();
    trace!(
        "Successfully obtained recipient addresses: {}.",
        plain_email_addresses.join(", ")
    );
    Ok(plain_email_addresses)
}

/// Parse the address with angle brackets and return it without them.
///
/// For now checking `from`. If it is not present, then fail.
///
fn parse_sender_address(mime: &::email::MimeMessage) -> Result<String> {
    trace!("Parsing sender address...");
    let from: String = mime
        .headers
        .get("From".to_owned())
        .ok_or(Error::NoFromError)?
        .get_value()?;
    let name_addr = from.parse::<::email::Mailbox>()?;
    let sender_address = name_addr.address;
    trace!(
        "Successfully obtained sender addresses: {}.",
        sender_address
    );
    Ok(sender_address)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn parse_email_no_from() {
        let email =
            fs::read_to_string("tests/data/email_plain_no_from.eml").unwrap();
        let r = Incoming::new(email);
        assert_eq!(
            "Error parsing Email: No From header",
            format!("{:#}", r.expect_err("No To header"))
        );
    }

    #[test]
    fn parse_email_name_with_comma() -> Result<()> {
        let email =
            fs::read_to_string("tests/data/email_plain_name_with_comma.eml")?;
        let email = Incoming::new(email)?;
        assert_eq!(1, email.recipients().iter().count());
        assert_eq!("test@mail.example", email.recipients()[0]);
        Ok(())
    }

    #[test]
    fn parse_email_addr_spec_from() {
        let email =
            fs::read_to_string("tests/data/email_plain_addr_spec_from.eml")
                .unwrap();
        let incoming = Incoming::new(email).unwrap();
        assert_eq!(incoming.from, "application@localhost");
    }

    #[test]
    fn parse_email_no_date() {
        let email =
            fs::read_to_string("tests/data/email_plain_no_date.eml").unwrap();
        let r = Incoming::new(email);
        assert_eq!(
            "Error parsing Email: No Date header",
            format!("{:#}", r.expect_err("No date header"))
        );
    }

    #[test]
    fn parse_email_no_to() {
        let email =
            fs::read_to_string("tests/data/email_plain_no_to.eml").unwrap();
        let r = Incoming::new(email);
        assert_eq!(
            "Error parsing Email: No To header",
            format!("{:#}", r.expect_err("No To header"))
        );
    }

    #[test]
    fn parse_email_no_subject() {
        let email =
            fs::read_to_string("tests/data/email_plain_no_subject.eml")
                .unwrap();
        let r = Incoming::new(email);
        assert!(r.is_ok());
    }

    #[test]
    fn parse_email_succeed() {
        let email = fs::read_to_string("tests/data/email_plain.eml").unwrap();
        let r = Incoming::new(email);
        assert!(r.is_ok());
    }

    #[test]
    fn plaintext() {
        let email = fs::read_to_string("tests/data/email_plain.eml").unwrap();
        let incoming = Incoming::new(email).unwrap();
        let plaintext = incoming.plaintext();
        assert_eq!(plaintext.headers.len(), 1);
        let ct = plaintext
            .headers
            .get_value::<String>("Content-Type".to_string())
            .unwrap();
        assert_eq!(ct, "text/plain; charset=utf-8");
        assert_eq!(plaintext.children.len(), 0);
        assert_eq!(plaintext.body, "Test body.\n\n");
    }

    #[test]
    fn parse_recipients_addresses_succeed() {
        let email_string =
            fs::read_to_string("tests/data/email_plain.eml").unwrap();
        let mime = ::email::MimeMessage::parse(email_string.as_str()).unwrap();
        let addresses = parse_recipients_addresses(&mime).unwrap();
        assert_eq!(vec!["user@localhost", "foo@localhost"], addresses);
    }
}
