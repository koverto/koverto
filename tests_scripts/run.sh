#!/bin/bash

NUMBER_EMAILS=10
# Mail will be stored in $HOME/mail, so do not run setting home to TMP_PATH
#TMP_PATH=/tmp
#cp .koverto.toml $TMP_PATH
cp -af ../tests/data/.koverto $HOME/
mkdir -p $HOME/mail

cargo run -- mta&
# It might take more time to start
sleep 5
# If the MTA has not started yet, this will error and emails won't be sent
./send_many_emails.sh 10
if [ $(ls $HOME/mail | wc -l) != 10 ]; then
     echo "Emails were not sent or received."
     exit 1
fi

# If the emails are not sent (yet), this won't do anything
cargo run&
sleep 5
killall koverto

#rm -rf $TMP_PATH/.koverto
#rm $TMP_PATH/.koverto.toml

# Until the data dir and the email queue dir can be different,
# do not remove the home data dir
#rm -rf $HOME/.koverto

echo "Tests scripts finished."
if [ "$(ls -A $HOME/mail)" ]; then
     echo "Email queue dir is not empty."
     rm -rf $HOME/mail
     exit 1
else
    echo "Email queue dir is empty."
    exit 0
fi
