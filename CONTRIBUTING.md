CONTRIBUTING
=============

Thank you for your interest in `koverto`.

Contributor license agreement
------------------------------

By submitting code you agree to the p≡p foundation's [CLA].

  [CLA]: https://contribution.pep.foundation/contribute/

Please direct questions regarding the CLA to [contribution@pep.foundation].

  [contribution@pep.foundation]: mailto:contribution@pep.foundation

Security vulnerability disclosure
---------------------------------

Please report suspected security vulnerabilities in private to the
[maintainers](maintainers.md).
Please do NOT create publicly viewable issues for suspected security
vulnerabilities.

Code of conduct
----------------

We want to create a welcoming environment for everyone who is interested in contributing. Please visit our [Code of Conduct](CODE_OF_CONDUCT.md) page to learn more about our commitment to an open and welcoming environment.

Contribution Flow
------------------

When contributing to koverto, your merge request is subject to review by maintainers.

When you submit code to koverto, we really want it to get merged, but there will be times when it will not be merged.

When maintainers are reading through a merge request they may request guidance from other maintainers. If merge request maintainers conclude that the code should not be merged, our reasons will be fully disclosed. If it has been decided that the code quality is not up to koverto’s standards, the merge request maintainer will refer the author to our docs and code style guides, and provide some guidance.

Sometimes style guides will be followed but the code will lack structural integrity, or the maintainer will have reservations about the code’s overall quality. When there is a reservation the maintainer will inform the author and provide some guidance. The author may then choose to update the merge request. Once the merge request has been updated and reassigned to the maintainer, they will review the code again. Once the code has been resubmitted any number of times, the maintainer may choose to close the merge request with a summary of why it will not be merged, as well as some guidance. If the merge request is closed the maintainer will be open to discussion as to how to improve the code so it can be approved in the future.

Gitlab allows marking merge requests as "work in progress" (WIP).
Commits with messages including 'wip', 'fixup' and some other terms
result in the merge request being marked as WIP.
Please rebase to squash such commits and force push an updated version.
Afterwards you can switch off the WIP state
to indicate the merge request is ready for review.


Merge requests workflow
-----------------------

See [merge requests](merge_requests.md)

Style guides
-------------

See [code style](code_style.md)


This file is based on https://docs.gitlab.com/ee/development/contributing/
