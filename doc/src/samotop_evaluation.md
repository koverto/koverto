# Samotop evaluation

- No much activity
- It uses tokio and it's a better start than starting with tokio from scratch
- It does not implement authentication yet
- STARTLS works
- An SMTP session works
- It does not send email to other servers yet, but that's not needed
- Investigate the way to access to the Mail data after the SMTP session is
  finished

An [Samotop session](samotop_session.md).
