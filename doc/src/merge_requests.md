Merge requests guidelines
=========================

The workflow to make a merge request (MR) is as follows:

1. Optional: Create an issue in [issues]
2. Fork the project into your personal namespace (or group)
3. Clone the [repository] locally
4. Install or run the code or documentation as explained in
   [INSTALL](install.md)
5. Create a feature branch in your local fork from `mainline` branch.
4. Write code [code style](code_style.md), tests, documentation,
   [commit], etc.
5. Ensure tests pass
6. Push your branch to your repository. Ensure CI pass
7. Create a MR from your branch

Your MR will be then be reviewed by [maintainers](../../maintainers.md).

[issues]: https://gitlab.com/juga0/koverto/issues
[repository]: https://gitlab.com/juga0/koverto
[commit]: https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html


Review
-------

When a MR is being reviewed, new changes might be needed:

- If the change does not modify a previous change, create new commits and
  push.
- If the change modifies a previous change and it's small,
  [git commit fixup](https://git-scm.com/docs/git-commit#git-commit---fixupltcommitgt)
  should be used.
  When it is agreed that the MR is ready, run:

  ``` {.sourceCode .bash}
  git rebase --autosquash
  ```

- If the review takes long and when it's ready code related to the MR has
  changed in mainline, you can create a new branch and run:

  ``` {.sourceCode .bash}
  git rebase mainline
  ```

  And close the old one pointing it to the new one.

Reviewing code {#review-ref}
--------------

The reviewers:

- Should let the contributor know what to improve/change.
- Should not push code to the contributor's branch without asking first.
- Should wait for contributor's changes or feedback after changes are
  requested, before merging or closing a MR.
- Should merge (not rebase) the MR. (see TODO)
- If new changes are needed when the contributor's branch is ready to
  merge, the reviewer can create a new branch based on the
  contributor's branch, push the changes and merge that MR. The
  contributor should be notified about it.
- If the reviewer realize that new changes are needed after the MR has
  been merged, the reviewer can push to mainline, notifying the
  contributor about the changes.
- Should not push directly to mainline, unless changes are trivial
  (typos, extra spaces, etc.)
- Should not push to mainline new features while there are open MRs to
  review.

TODO: decide on merge vs rebase workflow

[merging vs rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)

