
MTA servers implemented in Rust
===============================

By quick [Github Search]:

- [Samotop](https://docs.rs/samotop/0.7.4/samotop/): see
  [Samotop evaluation](samotop_evaluation.md)
- [new-tokio-smtp](https://github.com/dac-gmbh/new-tokio-smtp):
  started and continued to 2018
- [smtp-server](https://github.com/VictorKoenders/smtp_server):
  started in july 2019
- [yuubind](https://github.com/Ekleog/yuubind):
  started in 2018, continues in 2019
- [mail-smtp](https://docs.rs/crate/mail-smtp/):
  last commit in 2018. Repository says `DELETE`
- [smtpd](https://github.com/codeslinger/smtpd):
  last commit in 2014
- [tokio-smtp](https://github.com/stephank/tokio-smtp/), it's a client

Only Samotop and tokio-smtp have published crates.

More searches could be done, though there seems to be nothing stable or solid
yet.

Update on Jan 2019: [mailin](https://gitlab.com/alienscience/mailin/):
exactly what we were looking for months ago.

Possible Alternatives
---------------------

Use Python `smtpd` from Rust

[Github search]: https://github.com/search?l=&p=2&q=smtp+language%3ARust&ref=advsearch&type=Repositories

Use external SMTP server.
