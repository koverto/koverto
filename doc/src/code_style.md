Code style
===========

Lines must not exceed 79 characters, because not everyone has the privilege of
having a big monitor/screen.

Use whole words for variables names when possible to make the code more
readable.

Avoid slang or uncommon words, since not everyone is a English native speaker.

Any question or confusion from any user means that more/better documentation
is missing.


Follow the guidelines provided by [fmt-rfcs] until
there are official Rust style guidelines.


[fmt-rfcs]: https://github.com/rust-dev-tools/fmt-rfcs

To automatically detect code style issues, you can use `rustfmt` and
`clippy`, by running :

```
cargo install clippy
cargo install rustfmt
cargo clippy
cargo fmt
```

Comments codetags
-----------------
<!-- Part of #60 -->
[Codetags](https://en.wikipedia.org/wiki/Comment_(computer_programming)#Tags)
are used in different ways in different projects and it doesn't seem to be
consensus about them.
[PEP350](https://www.python.org/dev/peps/pep-0350/) is a not approved proposal.

For now we've being using the following codetags in the comments:

1. XXX: something that needs to be fixed in the future but it's not urgent.
   It usually doesn't have a ticket/issue for it, maybe cause is something
   very specific or it takes more time to create the issue/ticket.
2. TODO: something that needs to be implemented in the future.
   Maybe doesn't have a ticket/issue.
3. FIXME: someting that needs to be fixed soon, probably before merging.
   Don't usually have a ticket/issue.

When a codetag gets an issue/ticket, it should be replaced by the issue/ticket
number, eg `#38`.
When it's solved, either remove it or update the comment that follows the
issue/ticket.

Reasons why they can be useful:

- See related issue when looking at/working on a particular piece of the code,
  which otherwise would not be seen searching in a issue tracker because would
  not have known what to search for.
- To work offile
- To keep some information about the issues in the code that otherwise might
  get lost when changing Web platform/issue tracker.
