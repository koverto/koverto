//! Test for koverto as a queue processor
//!
//! These tests start koverto services via the command line API,
//! start a MUA to receive mails from koverto,
//! copy a mail to the queue,
//! and then check for received mails.

mod common;

use common::{fetch_mail, wait_for_processing, Koverto, SmtpSink};
use std::io::prelude::*;
use std::{ffi, fs, path, process, thread, time};

#[test]
fn process_moved_mail() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Koverto::new(Some(destination.listen_on));
    let keyring = proxy.home.path().join("keyring");
    proxy.exec(&["import", keyring.to_str().unwrap()]);
    proxy.start();
    wait_for_processing(&proxy.queue(), || {
        move_mail(&proxy.queue());
    });
    thread::sleep(time::Duration::from_millis(100));
    assert!(fetch_mail(&proxy.queue()).is_none());
    assert!(fetch_mail(&destination.queue()).is_some());
}

#[test]
fn only_process_closed_file() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Koverto::new(Some(destination.listen_on));
    let keyring = proxy.home.path().join("keyring");
    proxy.exec(&["import", keyring.to_str().unwrap()]);
    proxy.start();
    // ensure koverto is up and has processed the first batch
    wait_for_processing(&proxy.queue(), || {
        move_mail(&proxy.queue());
    });
    // the file is only kept open until this goes out of scope.
    let _open = open_mail(&proxy.queue());
    wait_for_processing(&proxy.queue(), || {
        move_mail(&proxy.queue());
    });
    let remaining = fetch_mail(&proxy.queue()).unwrap().unwrap();
    assert_eq!(
        &remaining.path(),
        &proxy.queue().join("mail_being_written.eml")
    );
}

#[test]
fn encrypt_mail() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Koverto::new(Some(destination.listen_on));
    let keyring = proxy.home.path().join("keyring");
    proxy.exec(&["import", keyring.to_str().unwrap()]);
    proxy.start();
    wait_for_processing(&proxy.queue(), || {
        copy_mail(&proxy.queue());
    });
    thread::sleep(time::Duration::from_millis(100));
    assert!(fetch_mail(&proxy.queue()).is_none());
    assert!(fetch_mail(&destination.queue()).is_some());
}

#[test]
fn drop_mail_if_key_is_missing() {
    let mut destination = SmtpSink::new();
    destination.start();
    let mut proxy = Koverto::new(Some(destination.listen_on));
    proxy.start();
    wait_for_processing(&proxy.queue(), || {
        copy_mail(&proxy.queue());
    });
    assert!(fetch_mail(&proxy.queue()).is_none());
    assert!(fetch_mail(&destination.queue()).is_none());
}

/// Use cp to 'send a mail'.
///
/// Mail has `from` addresses koverto is configured for.
fn copy_mail<P>(queue: P)
where
    P: AsRef<ffi::OsStr>,
    P: AsRef<path::Path>,
{
    std::fs::create_dir_all(&queue).unwrap();
    let copy = process::Command::new("cp")
        .args(&["tests/data/email_plain_single.eml"])
        .args(&[queue])
        .output()
        .expect("failed to copy mail");
    assert!(copy.status.success(), "{:?}", copy);
}

/// Use mv to 'send a mail'.
///
/// Mail has `from` addresses koverto is configured for.
fn move_mail<P>(queue: P)
where
    P: AsRef<ffi::OsStr>,
{
    let temp = tempfile::TempDir::new().unwrap();
    copy_mail(&temp.path());
    let mails = temp.path().join("email_plain_single.eml");
    let mv = process::Command::new("mv")
        .args(&[mails])
        .args(&[queue])
        .output()
        .expect("failed to mv mail");
    assert!(mv.status.success(), "{:?}", mv);
}

/// Open a file in the queue as if sending a mail was in progress.
///
/// A single line is written to the open file.
/// So when koverto tries to process it there will be an error.
fn open_mail<P>(queue: P) -> fs::File
where
    P: AsRef<path::Path>,
{
    std::fs::create_dir_all(&queue).unwrap();
    let filename = queue.as_ref().join("mail_being_written.eml");
    let mut file = fs::File::create(&filename).unwrap();
    file.write_all(b"To: <user@localhost>").unwrap();
    file
}
