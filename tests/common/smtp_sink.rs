use super::stdout;
use nix::unistd;
use rand::Rng;
use std::{ffi, iter, path, process};

const MISSING_MAIL_SINK: &str = "
    Could not find mail-sink. It should be compiled with koverto.
";

/// An SMTP sink to send mails to in tests
///
/// Creates a TempDir as queue.
/// Process will be killed when the struct goes out of scope.
pub struct SmtpSink {
    proc: Option<process::Child>,
    pub home: tempfile::TempDir,
    pub listen_on: u16,
}

impl SmtpSink {
    pub fn new() -> Self {
        Self {
            proc: None,
            home: tempfile::tempdir().unwrap(),
            listen_on: rand::thread_rng().gen_range(25000, 25999),
        }
    }

    /// Start the smtp_sink process
    pub fn start(&mut self) {
        self.proc = if self.running_as_root() {
            self.change_temp_owner("nobody").unwrap();
            Some(self.run(&["--user", "nobody"]))
        } else {
            Some(self.run(iter::empty::<&str>()))
        };
    }

    /// Queue dir of this smtp-sink instance.
    pub fn queue(&self) -> path::PathBuf {
        self.home.path().join("new")
    }

    fn run<I, S>(&mut self, args: I) -> process::Child
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        let result = process::Command::new("target/debug/mail-sink")
            .args(args)
            .arg("--maildir")
            .arg(format!("{}/", self.home.path().display()))
            .arg("-a")
            .arg(format!("127.0.0.1:{}", self.listen_on))
            .stdout(stdout())
            .spawn();
        match result {
            Ok(process) => process,
            Err(error) => panic!(
                "Could not start mail-sink: {}\n{}",
                error, MISSING_MAIL_SINK,
            ),
        }
    }

    fn running_as_root(&self) -> bool {
        unistd::geteuid().is_root()
    }

    fn change_temp_owner(&self, owner: &str) -> Result<(), nix::Error> {
        let user = users::get_user_by_name(owner).unwrap();
        let uid = unistd::Uid::from_raw(user.uid());
        let gid = unistd::Gid::from_raw(user.primary_group_id());
        unistd::chown(self.home.path(), Some(uid), Some(gid))
    }
}

impl Drop for SmtpSink {
    fn drop(&mut self) {
        for proc in self.proc.iter_mut() {
            proc.kill().unwrap();
        }
    }
}
