extern crate inotify;
extern crate rand;

mod koverto;
mod smtp_sink;

use inotify::{Inotify, WatchMask};
pub use koverto::Koverto;
pub use smtp_sink::SmtpSink;
use std::{env, fs, io, path, process};

pub fn wait_for_processing<P, F>(queue: P, func: F)
where
    P: AsRef<path::Path>,
    F: FnOnce(),
{
    fs::create_dir_all(&queue).unwrap();
    let mut inotify =
        Inotify::init().expect("Failed to initialize an inotify instance");
    inotify
        .add_watch(&queue, WatchMask::DELETE)
        .expect("Failed to add mail dir watch");
    let mut buffer = [0u8; 4096];
    func();
    inotify
        .read_events_blocking(&mut buffer)
        .expect("Error while waiting for mails");
}

/// Look for a mail in the queue belonging to home
///
/// Returns the first DirEntry in the folder if any is found.
pub fn fetch_mail<P>(queue: P) -> Option<io::Result<fs::DirEntry>>
where
    P: AsRef<path::Path>,
{
    match fs::read_dir(queue) {
        Ok(mut dir) => dir.next(),
        Err(_) => None,
    }
}

fn stdout() -> process::Stdio {
    if debug() {
        process::Stdio::inherit()
    } else {
        process::Stdio::null()
    }
}

fn log_level() -> &'static str {
    if debug() {
        "DEBUG"
    } else {
        "WARN"
    }
}

fn debug() -> bool {
    env::var("DEBUG").is_ok()
}
